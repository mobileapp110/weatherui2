import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'weather.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [DeviceSection()],
      builder: (context) {
        return MaterialApp(
          useInheritedMediaQuery: true,
          debugShowCheckedModeBanner: false,
          title: 'Flutter',
          theme: ThemeData(),
          home: const Scaffold(
            body: WeatherUI(),
          ),
        );
      },
    );
  }
}
