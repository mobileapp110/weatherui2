import 'package:flutter/material.dart';

void main() {
  runApp(const WeatherUI());
}

class WeatherUI extends StatelessWidget {
  const WeatherUI({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UI: Weather',
      useInheritedMediaQuery: true,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        iconTheme:
            const IconThemeData(color: Color.fromARGB(255, 255, 208, 52)),
        dividerTheme: const DividerThemeData(
            color: Colors.white, indent: 10, endIndent: 10),
      ),
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 108, 160, 237),
        body: buildBody(),
      ),
    );
  }

  Widget buildBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.blue,
            image: const DecorationImage(
                image: NetworkImage(
                    "https://www.guidingtech.com/wp-content/uploads/iOS-16-Weather-Wallpaper-2.jpg"),
                fit: BoxFit.fill),
          ),
        ),
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 5.0),
                child: const Text(
                  "Amphoe Mueang Chon Buri",
                  style: TextStyle(
                      fontSize: 26,
                      color: Colors.white,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
                child: const Text(
                  "32˚",
                  style: TextStyle(
                      fontSize: 70,
                      color: Colors.white,
                      fontWeight: FontWeight.w200),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
                child: const Text(
                  "Mostly Sunny",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 30.0),
                child: const Text(
                  "H:32˚ L:21˚",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: buildCard1(),
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: buildCard2(),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget buildCard1() {
    return SizedBox(
      height: 180,
      child: Card(
        color: const Color.fromARGB(255, 97, 155, 255),
        child: Column(
          children: [
            const ListTile(
              title: Text(
                "Sunny condition will continue for the rest of the day. Wind gusts are up to 19km/h.",
                style: TextStyle(color: Colors.white),
              ),
            ),
            const Divider(),
            buildWeather(),
          ],
        ),
      ),
    );
  }

  Widget buildWeather() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: const <Widget>[
            Text(
              "Now\n",
              style: TextStyle(color: Colors.white),
            ),
            Icon(Icons.sunny),
            Text(
              "\n32˚",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
        Column(
          children: const <Widget>[
            Text(
              "15\n",
              style: TextStyle(color: Colors.white),
            ),
            Icon(Icons.sunny),
            Text(
              "\n32˚",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
        Column(
          children: const <Widget>[
            Text(
              "16\n",
              style: TextStyle(color: Colors.white),
            ),
            Icon(Icons.sunny),
            Text(
              "\n31˚",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
        Column(
          children: const <Widget>[
            Text(
              "17\n",
              style: TextStyle(color: Colors.white),
            ),
            Icon(Icons.sunny),
            Text(
              "\n30˚",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
        Column(
          children: const <Widget>[
            Text(
              "17:55\n",
              style: TextStyle(color: Colors.white),
            ),
            Icon(Icons.wb_twighlight),
            Text(
              "\nSunset",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildCard2() {
    return SizedBox(
      // height: 500, //800
      child: Card(
        color: Color.fromARGB(255, 97, 155, 255),
        child: Column(
          children: [
            const ListTile(
              leading: Icon(Icons.calendar_month_outlined, color: Colors.white),
              title: Text(
                "10-DAY FORECAST",
                style: TextStyle(color: Colors.white),
              ),
            ),
            const Divider(),
            buildDay1(),
            const Divider(),
            buildDay2(),
            const Divider(),
            buildDay3(),
            const Divider(),
            buildDay4(),
            const Divider(),
            buildDay5(),
            const Divider(),
            buildDay6(),
            const Divider(),
            buildDay7(),
            const Divider(),
            buildDay8(),
            const Divider(),
            buildDay2(),
            const Divider(),
            buildDay3(),
          ],
        ),
      ),
    );
  }
}

Widget buildDay1() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Today    ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "22°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "31°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay2() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Sat      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "18°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "30°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay3() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Sun      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "18°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "30°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay4() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Mon      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "19°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "30°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay5() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Tue      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "21°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "31°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay6() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Wed      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "20°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "31°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay7() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Thu      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "20°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "30°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}

Widget buildDay8() {
  return Container(
    margin: const EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // ignore: prefer_const_literals_to_create_immutables
      children: <Widget>[
        const Text(
          "Fri      ",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.sunny,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "18°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        const Icon(
          Icons.linear_scale_outlined,
          color: Color.fromARGB(255, 255, 208, 52),
        ),
        const Text(
          "26°",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}
